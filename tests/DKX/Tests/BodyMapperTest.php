<?php

declare(strict_types=1);

namespace DKX\Tests;

use DKX\SlimBodyMapper\BodyMapper;
use DKX\SlimBodyMapper\Exception\EmptyRequestDataException;
use DKX\SlimBodyMapper\Exception\InnerDTOClassDoesNotExists;
use DKX\SlimBodyMapper\Exception\InvalidDocBlockTypeException;
use DKX\SlimBodyMapper\Exception\InvalidRequestDataException;
use DKX\SlimBodyMapper\Exception\NotArrayRequestBodyException;
use DKX\SlimBodyMapper\UploadedFile;
use DKX\SlimInjectableRoutes\InjectableRoutes;
use DKX\TestClasses\AddressBodyDTO;
use DKX\TestClasses\CreateUserRoute;
use DKX\TestClasses\CustomNameBodyDTO;
use DKX\TestClasses\CustomNameRoute;
use DKX\TestClasses\FileUploadBodyDTO;
use DKX\TestClasses\FileUploadRoute;
use DKX\TestClasses\LabelBodyDTO;
use DKX\TestClasses\UserBodyDTO;
use Mockery;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class BodyMapperTest extends TestCase
{


	/** @var \DKX\SlimBodyMapper\BodyMapper */
	private $bodyMapper;

	/** @var \DKX\SlimInjectableRoutes\InjectableRoutes */
	private $routes;

	/** @var \Psr\Http\Message\ServerRequestInterface|\Mockery\MockInterface */
	private $request;

	/** @var \Psr\Http\Message\ResponseInterface */
	private $response;



	public function setUp(): void
	{
		parent::setUp();

		$this->bodyMapper = new BodyMapper;
		$this->routes = new InjectableRoutes;
		$this->request = Mockery::mock(ServerRequestInterface::class);
		$this->response = Mockery::mock(ResponseInterface::class);

		$this->bodyMapper->setValidator($this->createValidator());
		$this->routes->provideInjectableSetup($this->bodyMapper->getInjectableSetup());
	}


	public function testCreateDTO_missingData(): void
	{
		self::expectException(EmptyRequestDataException::class);
		self::expectExceptionMessage('HTTP request does not contain any data');

		$this->request->shouldReceive('getParsedBody')->andReturn(null);
		$this->routes->__invoke([new CreateUserRoute, '__invoke'], $this->request, $this->response, []);
	}


	public function testCreateDTO_dataNotArray(): void
	{
		self::expectException(NotArrayRequestBodyException::class);
		self::expectExceptionMessage('HTTP request returned "object" but an array was expected');

		$this->request->shouldReceive('getParsedBody')->andReturn(new class {});
		$this->routes->__invoke([new CreateUserRoute, '__invoke'], $this->request, $this->response, []);
	}


	public function testCreateDTO_invalidDocBlock(): void
	{
		self::expectException(InvalidDocBlockTypeException::class);
		self::expectExceptionMessage(UserBodyDTO::class. '::$invalidDocBlock: unsupported docblock type "null,array,\DKX\TestClasses\AddressBodyDTO". Only class name (with null) is supported here');

		$this->request->shouldReceive('getParsedBody')->andReturn([
			'invalidDocBlock' => [],
		]);

		$this->routes->__invoke([new CreateUserRoute, '__invoke'], $this->request, $this->response, []);
	}


	public function testCreateDTO_unknownClass(): void
	{
		self::expectException(InnerDTOClassDoesNotExists::class);
		self::expectExceptionMessage(UserBodyDTO::class. '::$unknownClass: docblock type contains unknown class "\DKX\TestClasses\UnknownClass"');

		$this->request->shouldReceive('getParsedBody')->andReturn([
			'unknownClass' => [],
		]);

		$this->routes->__invoke([new CreateUserRoute, '__invoke'], $this->request, $this->response, []);
	}


	public function testCreateDTO_withValidator_invalid(): void
	{
		$this->request->shouldReceive('getParsedBody')->andReturn([]);

		try {
			$this->routes->__invoke([new CreateUserRoute, '__invoke'], $this->request, $this->response, []);
		} catch (InvalidRequestDataException $e) {
			$errors = $e->getViolationsList();
			self::assertCount(3, $errors);
		}
	}


	public function testCreateDTO(): void
	{
		$this->request->shouldReceive('getParsedBody')->andReturn([
			'name' => 'John Doe',
			'email' => 'john@doe.com',
			'address' => [
				'city' => 'Prague',
				'street' => 'Lorem Ipsum 5',
				'zipCode' => 123456,
			],
			'labels' => [
				['title' => 'First'],
				['title' => 'Second'],
			],
			'roles' => ['normal', 'admin'],
		]);

		/** @var \DKX\TestClasses\UserBodyDTO $called */
		$called = $this->routes->__invoke([new CreateUserRoute, '__invoke'], $this->request, $this->response, []);

		self::assertInstanceOf(UserBodyDTO::class, $called);
		self::assertSame('John Doe', $called->name);
		self::assertSame('john@doe.com', $called->email);
		self::assertNotNull($called->address);
		self::assertInstanceOf(AddressBodyDTO::class, $called->address);
		self::assertSame('Prague', $called->address->city);
		self::assertSame('Lorem Ipsum 5', $called->address->street);
		self::assertSame(123456, $called->address->zipCode);
		self::assertCount(2, $called->labels);
		self::assertInstanceOf(LabelBodyDTO::class, $called->labels[0]);
		self::assertSame('First', $called->labels[0]->title);
		self::assertInstanceOf(LabelBodyDTO::class, $called->labels[1]);
		self::assertSame('Second', $called->labels[1]->title);
		self::assertEquals(['normal', 'admin'], $called->roles);
	}


	public function testCustomDataName(): void
	{
		$this->request->shouldReceive('getParsedBody')->andReturn([
			'aaa' => 'A',
			'b' => 'B',
		]);

		/** @var \DKX\TestClasses\CustomNameBodyDTO $called */
		$called = $this->routes->__invoke([new CustomNameRoute, '__invoke'], $this->request, $this->response, []);

		self::assertInstanceOf(CustomNameBodyDTO::class, $called);
		self::assertSame('A', $called->a);
		self::assertSame('B', $called->b);
	}


	public function testFileUpload(): void
	{
		$this->request->shouldReceive('getParsedBody')->andReturn([
			'file' => 'R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==',
		]);

		/** @var \DKX\TestClasses\FileUploadBodyDTO $called */
		$called = $this->routes->__invoke([new FileUploadRoute(), '__invoke'], $this->request, $this->response, []);

		self::assertInstanceOf(FileUploadBodyDTO::class, $called);
		self::assertInstanceOf(UploadedFile::class, $called->file);
	}


	private function createValidator(): ValidatorInterface
	{
		return Validation::createValidatorBuilder()
			->enableAnnotationMapping()
			->getValidator();
	}

}
