<?php

declare(strict_types=1);

namespace DKX\Tests;

use DKX\SlimBodyMapper\UploadedFile;
use PHPUnit\Framework\TestCase;

final class UploadFileTest extends TestCase
{


	public function testToString(): void
	{
		$file = new UploadedFile('/file');
		self::assertSame('/file', $file->__toString());
	}

}
