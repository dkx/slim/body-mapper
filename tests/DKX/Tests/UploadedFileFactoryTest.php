<?php

declare(strict_types=1);

namespace DKX\Tests;

use DKX\SlimBodyMapper\UploadedFile;
use DKX\SlimBodyMapper\UploadedFileFactory;
use PHPUnit\Framework\TestCase;

final class UploadedFileFactoryTest extends TestCase
{


	public function testCreateFromBase64(): void
	{
		$file = UploadedFileFactory::createFromBase64('R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==');
		self::assertInstanceOf(UploadedFile::class, $file);
	}

}
