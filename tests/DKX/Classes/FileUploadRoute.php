<?php

declare(strict_types=1);

namespace DKX\TestClasses;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class FileUploadRoute
{


	public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args, FileUploadBodyDTO $body): FileUploadBodyDTO
	{
		return $body;
	}

}
