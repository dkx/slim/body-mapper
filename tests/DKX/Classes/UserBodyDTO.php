<?php

declare(strict_types=1);

namespace DKX\TestClasses;

use DKX\SlimBodyMapper\MappedHttpRequestBody;
use Symfony\Component\Validator\Constraints as Assert;

final class UserBodyDTO implements MappedHttpRequestBody
{


	/**
	 * @var string
	 * @Assert\Type("string")
	 * @Assert\NotBlank
	 */
	public $name;


	/**
	 * @var string
	 * @Assert\Type("string")
	 * @Assert\NotBlank
	 * @Assert\Email
	 */
	public $email;


	/**
	 * @var \DKX\TestClasses\AddressBodyDTO
	 * @Assert\Valid
	 * @Assert\NotBlank
	 */
	public $address;


	/**
	 * @var \DKX\TestClasses\LabelBodyDTO[]
	 * @Assert\Valid
	 */
	public $labels = [];


	/**
	 * @var string[]
	 * @Assert\Type("array")
	 * @Assert\All({
	 *     @Assert\Type("string"),
	 *     @Assert\NotBlank
	 * })
	 */
	public $roles = [];


	/**
	 * @var null|array|\DKX\TestClasses\AddressBodyDTO
	 * @Assert\Valid
	 */
	public $invalidDocBlock;


	/**
	 * @var null|\DKX\TestClasses\UnknownClass
	 * @Assert\Valid
	 */
	public $unknownClass;

}
