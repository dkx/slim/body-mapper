<?php

declare(strict_types=1);

namespace DKX\TestClasses;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class CustomNameRoute
{


	public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args, CustomNameBodyDTO $body): CustomNameBodyDTO
	{
		return $body;
	}

}
