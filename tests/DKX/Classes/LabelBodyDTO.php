<?php

declare(strict_types=1);

namespace DKX\TestClasses;

use Symfony\Component\Validator\Constraints as Assert;

final class LabelBodyDTO
{


	/**
	 * @var string
	 * @Assert\Type("string")
	 */
	public $title;

}
