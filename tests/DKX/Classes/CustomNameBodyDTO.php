<?php

declare(strict_types=1);

namespace DKX\TestClasses;

use DKX\SlimBodyMapper\Annotations\Path;
use DKX\SlimBodyMapper\MappedHttpRequestBody;

final class CustomNameBodyDTO implements MappedHttpRequestBody
{


	/**
	 * @Path("aaa")
	 * @var string
	 */
	public $a;

	/**
	 * @var string
	 */
	public $b;

}
