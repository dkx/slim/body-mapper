<?php

declare(strict_types=1);

namespace DKX\TestClasses;

use DKX\SlimBodyMapper\Annotations as Mapping;
use DKX\SlimBodyMapper\MappedHttpRequestBody;

final class FileUploadBodyDTO implements MappedHttpRequestBody
{


	/**
	 * @Mapping\UploadedFile
	 * @var \Slim\Http\UploadedFile
	 */
	public $file;

}
