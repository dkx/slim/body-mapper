<?php

declare(strict_types=1);

namespace DKX\TestClasses;

use DKX\SlimBodyMapper\MappedHttpRequestBody;
use Symfony\Component\Validator\Constraints as Assert;

final class AddressBodyDTO implements MappedHttpRequestBody
{


	/**
	 * @var string
	 * @Assert\Type("string")
	 * @Assert\NotBlank
	 */
	public $city;


	/**
	 * @var string
	 * @Assert\Type("string")
	 * @Assert\NotBlank
	 */
	public $street;


	/**
	 * @var string
	 * @Assert\Type("int")
	 * @Assert\NotBlank
	 */
	public $zipCode;

}
