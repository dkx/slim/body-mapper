<?php

declare(strict_types=1);

/** @var \Composer\Autoload\ClassLoader $loader */
$loader = require __DIR__. '/../../vendor/autoload.php';

\Doctrine\Common\Annotations\AnnotationRegistry::registerLoader([$loader, 'loadClass']);
