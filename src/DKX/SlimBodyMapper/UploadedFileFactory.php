<?php

declare(strict_types=1);

namespace DKX\SlimBodyMapper;

use function base64_decode;
use DKX\SlimBodyMapper\Exception\InvalidBase64FileDataException;
use DKX\SlimBodyMapper\Exception\RuntimeException;

final class UploadedFileFactory
{


	public static function createFromBase64(string $data, ?string $dir = null): UploadedFile
	{
		if ($dir === null) {
			$dir = \sys_get_temp_dir();
		}

		$name = tempnam($dir, 'SBM');
		if ($name === false) {
			throw new RuntimeException('Could not get name for new temp file in '. $dir);
		}

		$handle = fopen($name, 'w');
		if ($handle === false) {
			throw new RuntimeException('Could not open temp file '. $name);
		}

		$file = base64_decode($data, true);
		if ($file === false) {
			throw InvalidBase64FileDataException::create();
		}

		fwrite($handle, $file);
		fclose($handle);

		return new UploadedFile($name);
	}

}
