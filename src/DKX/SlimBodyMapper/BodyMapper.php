<?php

declare(strict_types=1);

namespace DKX\SlimBodyMapper;

use function array_key_exists;
use DKX\MethodInjector\InjectionContext;
use DKX\MethodInjector\MethodInjector;
use DKX\MethodInjector\Providers\Provider;
use DKX\SlimBodyMapper\Annotations\UploadedFile;
use DKX\SlimBodyMapper\Annotations\Path;
use DKX\SlimBodyMapper\Exception\EmptyRequestDataException;
use DKX\SlimBodyMapper\Exception\InnerDTOClassDoesNotExists;
use DKX\SlimBodyMapper\Exception\InvalidDocBlockTypeException;
use DKX\SlimBodyMapper\Exception\InvalidRequestDataException;
use DKX\SlimBodyMapper\Exception\NotArrayRequestBodyException;
use Doctrine\Common\Annotations\AnnotationReader;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionClass;
use ReflectionProperty;
use Roave\BetterReflection\Reflection as BetterReflection;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class BodyMapper
{


	/** @var \Symfony\Component\Validator\Validator\ValidatorInterface|null */
	private $validator;

	/** @var \Doctrine\Common\Annotations\AnnotationReader */
	private $annotationReader;


	public function __construct()
	{
		$this->annotationReader = new AnnotationReader;
	}


	public function setValidator(ValidatorInterface $validator): void
	{
		$this->validator = $validator;
	}


	public function getInjectableSetup(): callable
	{
		return [$this, 'provideInjectableSetup'];
	}


	public function provideInjectableSetup(MethodInjector $injector, ServerRequestInterface $request): void
	{
		$injector->provideFactory(MappedHttpRequestBody::class, function(InjectionContext $ctx) use ($request) {
			$data = $request->getParsedBody();

			if ($data === null) {
				throw EmptyRequestDataException::create();
			}

			if (!is_array($data)) {
				throw NotArrayRequestBodyException::create(gettype($data));
			}

			return $this->createDTO(
				$data,
				(string) $ctx->getType()
			);
		}, [
			Provider::USE_FOR_CHILD => true,
		]);
	}


	/**
	 * @param mixed[] $data
	 * @param string $className
	 * @return object
	 * @throws \ReflectionException
	 */
	private function createDTO(array $data, string $className): object
	{
		$class = new ReflectionClass($className);
		$dto = $class->newInstanceWithoutConstructor();

		foreach ($class->getProperties() as $property) {
			/** @var \DKX\SlimBodyMapper\Annotations\Path|null $nameReflection */
			$nameReflection = $this->annotationReader->getPropertyAnnotation($property, Path::class);
			$realName = $nameReflection === null ? $property->getName() : $nameReflection->getValue();

			if (array_key_exists($realName, $data)) {
				$value = $data[$realName];

				if (\is_array($value)) {
					$value = $this->getMappedValue($class, $property, $value);
				} else {
					/** @var \DKX\SlimBodyMapper\Annotations\UploadedFile|null $fileReflection */
					$fileReflection = $this->annotationReader->getPropertyAnnotation($property, UploadedFile::class);

					if ($fileReflection !== null) {
						$value = UploadedFileFactory::createFromBase64($value, $fileReflection->getDir());
					}
				}

				$property->setValue($dto, $value);
			}
		}

		if ($this->validator !== null) {
			$errors = $this->validator->validate($dto);

			if (count($errors) > 0) {
				throw InvalidRequestDataException::create($className, $errors);
			}
		}

		return $dto;
	}


	/**
	 * @param \ReflectionClass $class
	 * @param \ReflectionProperty $property
	 * @param mixed[] $value
	 * @throws \ReflectionException
	 * @return mixed
	 */
	private function getMappedValue(ReflectionClass $class, ReflectionProperty $property, array $value)
	{
		$className = $class->getName();
		$types = $this->getDocBlockTypeStrings($property);
		$typesCount = count($types);

		if ($typesCount !== 1 && $typesCount !== 2) {
			throw InvalidDocBlockTypeException::create($className, $property->getName(), implode(',', $types));
		}

		if (($nullKey = array_search('null', $types, true)) !== false) {
			unset($types[$nullKey]);
		}

		/** @var string $type */
		$type = array_values($types)[0];
		$isArray = false;

		if (mb_substr($type, mb_strlen($type) - 2) === '[]') {
			$type = mb_substr($type, 0, mb_strlen($type) - 2);
			$isArray = true;
		}

		if (in_array($type, ['string', 'mixed', 'array', 'int', 'bool'], true)) {
			return $value;
		}

		if (!class_exists($type)) {
			throw InnerDTOClassDoesNotExists::create($className, $property->getName(), $type);
		}

		if ($isArray) {
			return \array_map(function ($arrayValue) use ($type) {
				return $this->createDTO($arrayValue, $type);
			}, $value);
		}

		return $this->createDTO($value, $type);
	}


	private function getDocBlockTypeStrings(ReflectionProperty $property): array
	{
		$className = $property->getDeclaringClass()->getName();
		$betterProperty = BetterReflection\ReflectionProperty::createFromName($className, $property->getName());
		return $betterProperty->getDocBlockTypeStrings();
	}

}
