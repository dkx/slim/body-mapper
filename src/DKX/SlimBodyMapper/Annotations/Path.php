<?php

declare(strict_types=1);

namespace DKX\SlimBodyMapper\Annotations;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
final class Path
{


	/** @var string */
	private $value;


	public function __construct(array $values)
	{
		$this->value = $values['value'];
	}


	public function getValue(): string
	{
		return $this->value;
	}

}
