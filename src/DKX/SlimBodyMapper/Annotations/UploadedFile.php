<?php

declare(strict_types=1);

namespace DKX\SlimBodyMapper\Annotations;

use Doctrine\Common\Annotations\Annotation\Attribute;
use Doctrine\Common\Annotations\Annotation\Attributes;
use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 * @Attributes({
 *     @Attribute("dir", type="string")
 * })
 */
final class UploadedFile
{


	/** @var string */
	private $dir;


	public function __construct(array $values)
	{
		$this->dir = $values['dir'] ?? \sys_get_temp_dir();
	}


	public function getDir(): string
	{
		return $this->dir;
	}

}
