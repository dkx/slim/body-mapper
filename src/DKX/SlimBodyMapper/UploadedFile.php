<?php

declare(strict_types=1);

namespace DKX\SlimBodyMapper;

use Slim\Http;

final class UploadedFile extends Http\UploadedFile
{


	/**
	 * Used for symfony/validation
	 * @return string
	 */
	public function __toString(): string
	{
		return $this->file;
	}

}
