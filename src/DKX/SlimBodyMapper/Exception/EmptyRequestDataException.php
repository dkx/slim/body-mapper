<?php

declare(strict_types=1);

namespace DKX\SlimBodyMapper\Exception;

final class EmptyRequestDataException extends \RuntimeException
{


	public static function create(): self
	{
		return new self('HTTP request does not contain any data');
	}

}
