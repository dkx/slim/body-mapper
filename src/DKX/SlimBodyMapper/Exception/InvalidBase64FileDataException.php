<?php

declare(strict_types=1);

namespace DKX\SlimBodyMapper\Exception;

final class InvalidBase64FileDataException extends RuntimeException
{


	public static function create(): self
	{
		return new self('Could not decode invalid base64 file data');
	}

}
