<?php

declare(strict_types=1);

namespace DKX\SlimBodyMapper\Exception;

final class NotArrayRequestBodyException extends \LogicException
{


	public static function create(string $receivedType): self
	{
		return new self('HTTP request returned "'. $receivedType. '" but an array was expected');
	}

}
