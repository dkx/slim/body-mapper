<?php

declare(strict_types=1);

namespace DKX\SlimBodyMapper\Exception;

final class InnerDTOClassDoesNotExists extends \LogicException
{


	public static function create(string $className, string $propertyName, string $mappingClass): self
	{
		return new self($className. '::$'. $propertyName. ': docblock type contains unknown class "'. $mappingClass. '"');
	}

}
