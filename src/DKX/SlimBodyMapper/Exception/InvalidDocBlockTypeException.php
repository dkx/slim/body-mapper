<?php

declare(strict_types=1);

namespace DKX\SlimBodyMapper\Exception;

final class InvalidDocBlockTypeException extends \LogicException
{


	public static function create(string $className, string $propertyName, string $type): self
	{
		return new self($className. '::$'. $propertyName. ': unsupported docblock type "'. $type. '". Only class name (with null) is supported here');
	}

}
