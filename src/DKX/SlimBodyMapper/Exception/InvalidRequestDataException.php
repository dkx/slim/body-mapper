<?php

declare(strict_types=1);

namespace DKX\SlimBodyMapper\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

final class InvalidRequestDataException extends \RuntimeException
{


	/** @var \Symfony\Component\Validator\ConstraintViolationListInterface */
	private $violationsList;


	public function __construct(string $message, ConstraintViolationListInterface $violationsList)
	{
		parent::__construct($message);

		$this->violationsList = $violationsList;
	}


	public static function create(string $className, ConstraintViolationListInterface $violationsList): self
	{
		$detail = '';

		if (method_exists($violationsList, '__toString')) {
			$detail = "\n\n". (string) $violationsList;
		}

		return new self($className. ': validation of mapped HTTP request data failed'. $detail, $violationsList);
	}


	public function getViolationsList(): ConstraintViolationListInterface
	{
		return $this->violationsList;
	}

}
